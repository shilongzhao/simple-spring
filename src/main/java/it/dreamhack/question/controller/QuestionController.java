package it.dreamhack.question.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.dreamhack.question.entity.Question;
import it.dreamhack.question.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api/questions", consumes = MediaType.ALL_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@Api
public class QuestionController {
    @Autowired
    private QuestionRepository questionRepository;

    @GetMapping
    @ApiOperation(value = "Find all questions", response = List.class)
    public List<Question> findAll() {
        List<Question> questions = new ArrayList<>();
        questionRepository.findQuestionsByIdAfter(0L).forEach(questions::add);
        return questions;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create question", response = Question.class)
    public Question create(@RequestBody Question question) {
        return questionRepository.save(question);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get question", response = Question.class)
    public Question getQuestionById(@PathVariable  Long id) {
        return questionRepository.findOne(id);
    }

    @PutMapping(path="/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update question", response = Question.class)
    public Question updateQuestionById(@PathVariable Long id, @RequestBody Question question) {
        if (questionRepository.findOne(id) == null || !question.getId().equals(id)) {
            return null;
        }
        return questionRepository.save(question);
    }

    //TODO: search by QuestionFilter
}
