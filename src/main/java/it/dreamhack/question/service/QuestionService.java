package it.dreamhack.question.service;

import it.dreamhack.question.entity.Question;
import it.dreamhack.question.pojo.QuestionFilter;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class QuestionService {
    @PersistenceContext
    private EntityManager em;

    public List<Question> filterQuestion(QuestionFilter filter) {
        return em.createQuery(" " +
                "SELECT q FROM Question q " +
                "   LEFT JOIN q.exam e " +
                "WHERE e.courseId LIKE concat('%', :courseId, '%') ", Question.class)
                .setParameter("courseId", filter.getCourseIdLike())
                .getResultList();
    }
}
