package it.dreamhack.question.repository;

import it.dreamhack.question.entity.Exam;
import org.springframework.data.repository.CrudRepository;

public interface ExamRepository extends CrudRepository<Exam, Long> {
}
