package it.dreamhack.question.repository;

import it.dreamhack.question.entity.Question;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    @EntityGraph(attributePaths = "exam")
    Iterable<Question> findQuestionsByIdAfter(Long id);
}
