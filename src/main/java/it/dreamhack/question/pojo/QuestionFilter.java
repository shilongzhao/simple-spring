package it.dreamhack.question.pojo;

public class QuestionFilter {
    private String courseIdLike;

    public String getCourseIdLike() {
        return courseIdLike;
    }

    public void setCourseIdLike(String courseIdLike) {
        this.courseIdLike = courseIdLike;
    }
}
