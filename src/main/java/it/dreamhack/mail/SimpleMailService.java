package it.dreamhack.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;

/**
 * the app email engine which send emails when
 * a user is billed, registered, batch job finishes,
 * system error happens, etc
 *
 */
public class SimpleMailService {
    public void send(String to, String subject, String text) {

    }

    public void send(String to, String subject, String text, String pathToAttachment) throws MessagingException {

    }
}
