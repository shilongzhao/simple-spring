package it.dreamhack.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.MailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * A separate thread for distributing app emails:
 * subscriptions, notifications, success registrations, etc.
 */
@Component
public class SimpleMailEngine {
    private static final Logger logger = LoggerFactory.getLogger(SimpleMailEngine.class);

    @JmsListener(destination = "mailbox")
    public void receiveMessage(String msg) {
        logger.debug("JMS message: " + msg);
    }

}
