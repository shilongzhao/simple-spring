package it.dreamhack.config;

/**
 * this configuration engine is used by app-admin instead of developers
 * where app-admin could configure business related configurations, i.e.
 * create/delete roles, grant/revoke permissions to an app-user, etc.
 *
 * some configuration maybe have a list structure,
 * each configuration should have a pointer to its previous configuration (key, value)
 * class ConfigVar {
 *     string key
 *     string value
 *     date modifydate
 *     ConfigVar lastVersion
 * }
 */
public class BusinessConfigurationEngine {
}
