package it.dreamhack.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.pojo.UserFilter;
import it.dreamhack.user.repository.UserRepository;
import it.dreamhack.user.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api/users", consumes = MediaType.ALL_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@Api
public class UserController {
    @Autowired
    private CustomUserDetailsService userDetailsService;
    @Autowired
    private UserRepository userRepository;

    @PostMapping
    @ApiOperation(value = "Create user, ADMIN required", response = User.class)
    public User createUser(@RequestBody User user) {
        // TODO: reCAPTCHA check, prevent malicious register
        return userDetailsService.registerUser(user);
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Show all users, ADMIN required", response = List.class)
    public List<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        userRepository.findAllByIdAfter(0L).forEach(users::add);
        return users;
    }

    @PostMapping(path = "/search")
    @ApiOperation(value = "Search users, ADMIN required", response = List.class)
    public List<User> search(@RequestBody UserFilter filter) {
        return userRepository.search(filter);
    }

    @GetMapping(path = "/{id}")
    public User getUserById(@PathVariable Long id) {
        return userRepository.findUserById(id);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update user", response = User.class)
    public User updateUserById(@PathVariable Long id, @RequestBody User inbound) {
        // TODO: check if they are the same user!
        return userRepository.save(inbound);
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Disable user", response = User.class)
    public User disableUserById(@PathVariable Long id) {
        return userRepository.disableUserById(id);
    }
}
