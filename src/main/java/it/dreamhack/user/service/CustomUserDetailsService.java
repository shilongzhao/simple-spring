package it.dreamhack.user.service;

import it.dreamhack.user.repository.RoleRepository;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * If a method is used in multiple places, then the method
 * should be put in a service bean. e.g. the user could be created by
 * admin or through a registration page, they both needs to set the
 * salt hashed password, notify the new user his account is to be activated,
 * ... so a userCreation method should be put inside this service bean
 */
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private JmsTemplate jmsTemplate;

    private JmsMessagingTemplate jmsMessagingTemplate;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username);
    }

    public User registerUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.grantRole(roleRepository.findOne("USER"));
        return userRepository.save(user);
    }

    public User granAdmin(User user) {
        user.grantRole(roleRepository.findOne("ADMIN"));
        return userRepository.save(user);
    }

}
