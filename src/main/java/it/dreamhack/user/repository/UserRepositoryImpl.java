package it.dreamhack.user.repository;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.pojo.UserFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * When Spring Data JPA generates the implementation for a repository interface,
 * it also looks for a class whose name is the same as the interface’s name post fixed with
 * Impl
 * If the class exists, Spring Data JPA merges its methods with those generated by Spring Data JPA.
 */
public class UserRepositoryImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private UserRepository userRepository;
    @Override
    public List<User> search(UserFilter filter) {
        return em.createQuery(" " +
                " SELECT u FROM User u " +
                "   WHERE (u.id = :id or :id is null) " +
                "       AND (u.name like concat('%',:name,'%') or :name is null) " +
                "       AND (u.email like concat('%',:email,'%') or :email is null)", User.class)
                .setParameter("id", filter.getId())
                .setParameter("name", filter.getName())
                .setParameter("email", filter.getEmail())
                .getResultList();
    }

    @Override
    public User disableUserById(Long id) {
        User target = userRepository.findOne(id);
        target.setEnabled(false);
        return userRepository.save(target);
    }

}
