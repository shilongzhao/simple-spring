package it.dreamhack.user.repository;

import it.dreamhack.user.entity.User;
import it.dreamhack.user.pojo.UserFilter;

import java.util.List;

public interface UserRepositoryCustom {
    List<User> search(UserFilter filter);
    User disableUserById(Long userId);
}
