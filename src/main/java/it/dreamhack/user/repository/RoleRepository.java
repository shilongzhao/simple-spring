package it.dreamhack.user.repository;

import it.dreamhack.user.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, String> {
}
