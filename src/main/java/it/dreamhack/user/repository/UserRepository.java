package it.dreamhack.user.repository;

import it.dreamhack.user.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>, UserRepositoryCustom {
    @EntityGraph(attributePaths = {"roles", "permissions"})
    User findUserByUsername(String username);
    @EntityGraph(attributePaths = {"roles", "permissions"})
    Iterable<User> findAllByIdAfter(Long id);
    @EntityGraph(attributePaths = {"roles", "permissions"})
    User findUserById(Long id);
}
