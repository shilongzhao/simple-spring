package it.dreamhack.user.repository;

import it.dreamhack.user.entity.Permission;
import org.springframework.data.repository.CrudRepository;


public interface PermissionRepository extends CrudRepository<Permission, String> {

}
