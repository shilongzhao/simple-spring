package it.dreamhack;

import it.dreamhack.question.entity.Exam;
import it.dreamhack.question.entity.Question;
import it.dreamhack.question.repository.ExamRepository;
import it.dreamhack.question.repository.QuestionRepository;
import it.dreamhack.user.entity.Role;
import it.dreamhack.user.repository.RoleRepository;
import it.dreamhack.user.entity.User;
import it.dreamhack.user.repository.UserRepository;
import it.dreamhack.user.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 11/12/2017
 */

@SpringBootApplication
@EnableAsync @EnableJms
public class Application {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CustomUserDetailsService userDetailsService;
    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private QuestionRepository questionRepository;

    /**
     * since we are using embedded servet container,
     * main function is needed
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // remove this in production
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            Arrays.stream("USER,ADMIN".split(",")).forEach(roleId ->{
                Role role = new Role();
                role.setId(roleId);
                roleRepository.save(role);
            });
            Arrays.stream("user,user1,user2".split(",")).forEach(username -> {
                User u = new User();
                u.setPassword("password");
                u.setUsername(username);
                u.setEmail(username+"@example.com");
                userDetailsService.registerUser(u);
            });

            Arrays.stream("admin,admin1,admin2".split(",")).forEach(username -> {
                User u = new User();
                u.setPassword("password");
                u.setUsername(username);
                u.setEmail(username+"@example.com");
                u = userDetailsService.registerUser(u);
                userDetailsService.granAdmin(u);
            });
            Arrays.stream("C,Alogrithm".split(",")).forEach(courseId -> {
                Exam e = new Exam();
                e.setCourseId(courseId);
                e.setExamDate(new Date());
                Question q1 = new Question();
                q1.setTitle("Game of Thrones");
                q1.setProblem("Who is John Snow's mother? " + courseId);
                e.getQuestions().add(q1);
                e = examRepository.save(e);
            });

        };
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
