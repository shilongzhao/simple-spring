package it.dreamhack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author: zhaoshilong
 * date: 11/12/2017
 */
@RestController
@RequestMapping(path = "/api", consumes = MediaType.ALL_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class HelloController {
    private static final Logger log = LoggerFactory.getLogger(HelloController.class);
    @Autowired
    private JmsTemplate jmsTemplate;

    @GetMapping("/hello")
    public String index() {
        log.debug("JMS here");
        jmsTemplate.convertAndSend("mailbox", "hello recorded");
        return "Greetings from Spring Boot!\n";
    }

}