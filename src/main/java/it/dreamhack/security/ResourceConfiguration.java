package it.dreamhack.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

// @EnableResourceServer annotation enables a Spring Security filter
// that authenticates requests via checking contents of incoming OAuth2 tokens
@Configuration
@EnableResourceServer
public class ResourceConfiguration extends ResourceServerConfigurerAdapter {
    @Value("${security.oauth2.resource.id}")
    private String resourceId;

    @Autowired
    private DefaultTokenServices tokenServices;

    @Autowired
    private TokenStore tokenStore;

    // To allow the rResourceServerConfigurerAdapter to understand the token,
    // it must share the same characteristics with AuthorizationServerConfigurerAdapter.
    // So, we must wire it up the beans in the ResourceServerSecurityConfigurer.
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
                .resourceId(resourceId)
                .tokenServices(tokenServices)
                .tokenStore(tokenStore);
    }

    // the HttpSecurity defined on ResourceConfiguration takes precedence over the one defined on SecurityConfiguration,
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
//                .requestMatcher(request -> request.getServletPath().startsWith("/api/hello"))
//                .csrf().disable()
//                .anonymous().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll() // Ant style URL matchers, you can also use regex style matchers
                // TODO: will be removed in future hello API
                .antMatchers("/api/users").access("hasAnyRole('ADMIN')")
                // restricting all access to /api/** to authenticated users
                //.antMatchers("/api/**").authenticated();
                .antMatchers("/api/**").permitAll(); //TEST

    }
}
