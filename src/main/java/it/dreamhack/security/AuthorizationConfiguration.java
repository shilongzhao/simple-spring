package it.dreamhack.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

/**
 * configure the OAuth, /oauth/token
 */
@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true)

/**
 * The URL paths provided by the framework are:
 *      - /oauth/authorize (the authorization endpoint),
 *      - /oauth/token (the token endpoint),
 *      - /oauth/confirm_access (user posts approval for grants here),
 *      - /oauth/error (used to render errors in the authorization server),
 *      - /oauth/check_token (used by Resource Servers to decode access tokens),
 *      - and /oauth/token_key (exposes public key for token verification if using JWT tokens).
 */
public class AuthorizationConfiguration extends AuthorizationServerConfigurerAdapter {
    private Logger logger = Logger.getLogger(AuthorizationConfiguration.class);

    private int accessTokenValiditySeconds = 10000;
    private int refreshTokenValiditySeconds = 30000;

    @Value("${security.oauth2.resource.id}")
    private String resourceId;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;
    /**
     *
     * @param endpoints AuthorizationEndpoint    is used to service requests for authorization. Default URL: /oauth/authorize.
     *                  TokenEndpoint           is used to service requests for access tokens. Default URL: /oauth/token.
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
                .accessTokenConverter(tokenConverter())
                .tokenServices(tokenServices())
                .tokenStore(tokenStore());

    }

    /**
     *
     * @param security      defines the security constraints on the token endpoint.
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                // we're allowing access to the token only for clients with 'ROLE_TRUSTED_CLIENT' authority
                .tokenKeyAccess("hasAuthority('ROLE_TRUSTED_CLIENT')")
                .checkTokenAccess("hasAuthority('ROLE_TRUSTED_CLIENT')");
    }

    /**
     * Client details can be updated in a running application by access the underlying store directly
     * (e.g. database tables in the case of JdbcClientDetailsService) or through the ClientDetailsManager interface
     * (which both implementations of ClientDetailsService also implement).
     *
     * We are using in memory ClientDetailsService since we have only one client.
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("trusted-app")
                .authorizedGrantTypes("client_credentials", "password", "refresh_token")
                .authorities("ROLE_TRUSTED_CLIENT")
                .scopes("read", "write")
                .resourceIds(resourceId)
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
                .refreshTokenValiditySeconds(refreshTokenValiditySeconds)
                //TODO: protect the secret!
                .secret("secret");
    }

    // persistence of tokens are delegated to TokenStore
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(tokenConverter());
    }

    // the Resource Server also has to be able to verify the signature,
    // so it either needs the same symmetric (signing) key as the Authorization Server (shared secret, or symmetric key),
    // or it needs the public key (verifier key) that matches the private key (signing key) in the
    // Authorization Server (public-private or asymmetric key). The public key (if available) is
    // exposed by the Authorization Server on the /oauth/token_key endpoint
    @Bean
    public JwtAccessTokenConverter tokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // TODO: keep this signing key safe!
        KeyStoreKeyFactory keyStoreKeyFactory =
                new KeyStoreKeyFactory(
                        new ClassPathResource("mykeys.jks"),
                        "mypass".toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mykeys"));
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenService = new DefaultTokenServices();
        tokenService.setTokenStore(tokenStore());
        tokenService.setSupportRefreshToken(true);
        tokenService.setTokenEnhancer(tokenConverter());
        return tokenService;
    }

}
